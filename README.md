# Find questions and sub types

This is a script that performs data preprocessing on a TSV(tab separated value) file to find if a query is a question or not. It also further categorizes these questions into three groups:

```
1. Yes or No questions
2. W or H questions
3. Indirect questions
```

The script is created for modularity and is easily modifiable to add more classes of questions. Assuming you have `Anaconda` distribution for `Python`, please run the following command from the root directory:

```
conda env create -f env.yml
```

Once the environment is created, please run the following command:

```
python preprocessing.py
```

This will run the script and two new files(already provided for convenience):
```
1. questions.tsv
2. subtypes.tsv
```
The first file provides a 0 or 1 in a new column if the query is a question or not. The second file provides a category of the questions in the query. The 3 categories were chosen based on the data provided, but can be modified as needed if the data changes.
