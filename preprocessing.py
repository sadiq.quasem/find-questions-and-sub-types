import pandas as pd

df = pd.read_csv('queries-10k-txt', sep='\t')
df_questions = df.drop_duplicates()
df_questions = df_questions.dropna()
df_questions = df_questions.drop(['FREQ'], axis = 1)

def find_questions(row):
    questions_list = ["what","when","where","which","who","whose","why","would", "how","is","are","do","can","could","should","did","does","will","was","were", "isn't","wasn't","hasn't","didn't", "wouldn't", "doesn't","couldn't","can't"]
    if row['QUERY'].split()[0] in questions_list:
        return 1
    else:
        return 0

def find_subtypes(row):
    yes_no = ["is","are","do","can","did","does","will","was","were"]
    w_h = ["what","when","where","which","who","whose","why", "how"]
    indirect = ["could","would","should"]
    '''
    negative = ["isn't","wasn't","hasn't","didn't", "wouldn't", "doesn't","couldn't","can't"]
    '''

    if row['QUERY'].split()[0] in yes_no:
        return 1
    elif row['QUERY'].split()[0] in w_h:
        return 2
    elif row['QUERY'].split()[0] in indirect:
        return 3
    '''
    elif row['QUERY'].split()[0] in negative:
        return 4
    '''
    
def main():
    df_questions['QUESTIONS'] = df_questions.apply(find_questions, axis=1)
    df_questions.to_csv('questions.tsv', sep='\t')

    df_questions_only = df_questions.loc[df_questions['QUESTIONS'] == 1]
    df_subtypes = df_questions_only

    df_subtypes['SUBTYPES'] = df_subtypes.apply(find_subtypes, axis=1)
    df_questions_only.to_csv('subtypes.tsv', sep='\t')

main()
